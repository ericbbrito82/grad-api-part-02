from fastapi import Depends, FastAPI, HTTPException
import uvicorn
from uuid import UUID, uuid4
from estilos.schemas import Student, StudentData, SetStudentCourse, Professor, Class, Course, StudentClass, SetCourseCoordinator, SetTeacherClass
from sqlalchemy.orm import Session
from estilos.models import Students, Professors, StudentsClasses, Classes, Courses
from functions.database import get_db

app = FastAPI(title="PROGRAD API")


# Students


@app.get(   # Buscar todos os estudantes
    '/students',
    summary='Fetch all students data',
    tags=['Students'],
)
def get_students(db: Session = Depends(get_db)):
    return db.query(Students).all()


@app.get(   # Buscar um estudante específico
    '/students/{student_id}',
    summary='Fetch student data',
    tags=['Students'],
)
def get_student(student_id: UUID, db: Session = Depends(get_db)):
    f = db.query(Students).filter(
        Students.id == student_id).first()
    if f is None:
        raise HTTPException(
            status_code=404, detail=f'Student with id={student_id} not found')
    return f


@app.post(   # Adicionar um novo estudante
    '/students',
    summary='Add a new student',
    tags=['Students'],
    status_code=201,
)
def add_student(student_data: StudentData, db: Session = Depends(get_db)):

    new_student = Students(**student_data.dict())

    db.add(new_student)
    db.commit()
    db.refresh(new_student)
    return new_student


@app.put(   # Alterar o curso de um estudante
    '/students/{students_id}/course/{course_id}',
    summary='Set or change a students course',
    tags=['Students'],
    status_code=201
)
def set_student_course(set_data: SetStudentCourse, db: Session = Depends(get_db)):
    f = db.query(Students).filter(
        Students.id == set_data.student_id).first()
    if f is None:
        raise HTTPException(
            status_code=404, detail=f'Student with id={set_data.student_id} not found')

    db.query(Students).filter(Students.id == set_data.student_id).update(
        {Students.course_id: set_data.new_course_id})
    db.commit()
    db.refresh(f)

    return f


@app.post(  # Matriculando um aluno numa matéria
    '/students/{students_id}/class/{class_id}',
    summary='Enroll student in a class',
    tags=['Students'],
    status_code=201
)
def enroll_student_class(enroll_data: StudentClass, db: Session = Depends(get_db)):
    new_enroll = StudentsClasses(**enroll_data.dict())

    db.add(new_enroll)
    db.commit()
    db.refresh(new_enroll)
    return new_enroll


@app.delete(   # Removendo uma materia de um estudante
    '/students/{students_id}/class/{class_id}',
    summary='Remove student from a class',
    tags=['Students'],
    status_code=201
)
def remove_student_class(remove_data: StudentClass, db: Session = Depends(get_db)):
    db.query(StudentsClasses).filter(StudentsClasses.student_id ==
                                     remove_data.student_id, StudentsClasses.class_id == remove_data.class_id).delete()
    db.commit()
    db.refresh()

    return print("Success")


# Professors


@app.get(   # Buscar todos os professores
    '/professors',
    summary='Fetch all professors data',
    tags=['Professors'],
)
def get_professors(db: Session = Depends(get_db)):
    return db.query(Professors).all()


@app.get(   # Buscar um professor específico
    '/professors/{professor_id}',
    summary='Fetch professor data',
    tags=['Professors'],
)
def get_professor(professor_id: UUID, db: Session = Depends(get_db)):
    f = db.query(Professors).filter(
        Professors.id == professor_id).first()
    if f is None:
        raise HTTPException(
            status_code=404, detail=f'Professor with id={professor_id} not found')
    return f


@app.post(   # Adicionar um novo professor
    '/professors',
    summary='Add a new professor',
    tags=['Professors'],
    status_code=201,
)
def add_professor(professor_data: Professor, db: Session = Depends(get_db)):

    new_professor = Professors(**professor_data.dict())

    db.add(new_professor)
    db.commit()
    db.refresh(new_professor)
    return new_professor


# Courses


@app.get(   # Buscar todos os cursos
    '/courses',
    summary='Fetch all courses data',
    tags=['Courses'],
)
def get_courses(db: Session = Depends(get_db)):
    return db.query(Courses).all()


@app.get(   # Buscar um curso específico
    '/courses/{course_id}',
    summary='Fetch course data',
    tags=['Courses'],
)
def get_course(course_id: UUID, db: Session = Depends(get_db)):
    f = db.query(Courses).filter(Courses.id == course_id).first()
    if f is None:
        raise HTTPException(
            status_code=404, detail=f'Course with id={course_id} not found')
    return f


@app.post(   # Adicionar um novo curso
    '/courses',
    summary='Add a new course',
    tags=['Courses'],
    status_code=201,
)
def add_course(course_data: Course, db: Session = Depends(get_db)):

    new_course = Courses(**course_data.dict())

    db.add(new_course)
    db.commit()
    db.refresh(new_course)

    return new_course


@app.get(   # Buscar alunos de um curso específico
    '/courses/{course_id}/students',
    summary='Fetch students of course',
    tags=['Courses'],
)
def get_students_course(course_id: UUID, db: Session = Depends(get_db)):
    return db.query(Students).filter(Students.course_id == course_id).all()


@app.put(   # Alterar o coordenador de um curso
    '/courses/{course_id}/coordinator/{professor_id}',
    summary='Set or change a professor as the course coordinator',
    tags=['Courses'],
    status_code=201
)
def set_course_coordinator(set_data: SetCourseCoordinator, db: Session = Depends(get_db)):
    db.query(Courses).filter(Courses.id == set_data.course_id).update(
        {Courses.coordinator_id: set_data.professor_id})
    db.commit()
    return db.query(Courses).filter(Courses.id == set_data.course_id).first()

# Classes


@app.get(   # Buscar todas as disciplinas
    '/classes',
    summary='Fetch all classes data',
    tags=['Classes'],
)
def get_classes(db: Session = Depends(get_db)):
    return db.query(Classes).all()


@app.get(   # Buscar uma disciplina específica
    '/classes/{class_id}',
    summary='Fetch a class data',
    tags=['Classes'],
)
def get_class(class_id: UUID, db: Session = Depends(get_db)):
    f = db.query(Courses).filter(Courses.id == class_id).first()
    if f is None:
        raise HTTPException(
            status_code=404, detail=f'Class with id={class_id} not found')
    return f


@app.post(   # Adicionar uma nova disciplina
    '/classes',
    summary='Add a new class',
    tags=['Classes'],
    status_code=201,
)
def add_class(class_data: Class, db: Session = Depends(get_db)):

    new_class = Classes(**class_data.dict())

    db.add(new_class)
    db.commit()
    db.refresh(new_class)
    return new_class


@app.get(   # Buscar os alunos de uma disciplina
    '/classes/{class_id}/students',
    summary='Fetch students of a class',
    tags=['Classes'],
)
def get_students_class(class_id: UUID, db: Session = Depends(get_db)):
    x = []
    for f in db.query(Students, Classes).filter(StudentsClasses.class_id == class_id, StudentsClasses.student_id == Students.id):
        x.append(f)
    return f


@app.put(   # Alterar o professor de uma disciplina
    '/classes/{classes_id}/teacher/{professor_id}',
    summary='Set or change a professor as the teacher of a class',
    tags=['Classes'],
    status_code=201
)
def set_teacher_class(set_data: SetTeacherClass, db: Session = Depends(get_db)):
    db.query(Classes).filter(Classes.id == set_data.class_id).update(
        {Classes.professor_id: set_data.professor_id})
    db.commit()
    return db.query(Classes).filter(Classes.id == set_data.class_id).first()


""" 
@app.get(   # Rankear as disciplinas de acordo com a quantidade de alunos
    '/classes/ranking',
    summary='Get a ranking of classes by the number of their enrolled students',
    tags=['Classes'],
    status_code=201
)
def set_classes_ranking(db: Session = Depends(get_db)):
    classes = db.query(Classes).all()
    students_classes = db.query(StudentsClasses).all()
    x = []
    for i in range(len(classes)):
        class_name = classes[i][1]
        count = 0
        for n in range(len(students_classes)):
            if students_classes[n][1] == classes[i][0]:
                count += 1
        x.append((class_name, count))
    f = sorted(x, key=lambda y: y[1], reverse=True)
    return f """


if __name__ == '__main__':
    uvicorn.run(app='grad:app', port=3000, reload=True)
