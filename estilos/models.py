from sqlalchemy import Column, ForeignKey, String, Date
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID
import uuid

from functions.database import Base


class Students(Base):
    __tablename__ = "students"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    cpf = Column(String, unique=True)
    name = Column(String)
    birth_date = Column(Date)
    rg_number = Column(String)
    rg_expediter = Column(String)
    rg_uf = Column(String)
    course_id = Column(UUID(as_uuid=True), ForeignKey("courses.id"))

    student_link = relationship(
        "StudentsClasses", back_populates="link_student")


class Courses(Base):
    __tablename__ = "courses"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    name = Column(String, unique=True)
    creation_date = Column(Date)
    bilding = Column(String)
    coordinator_id = Column(UUID(as_uuid=True), ForeignKey("professors.id"))


class Classes(Base):
    __tablename__ = "classes"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    name = Column(String, unique=True)
    code = Column(String)
    description = Column(String)
    professor_id = Column(UUID(as_uuid=True), ForeignKey("professors.id"))

    professor = relationship("Professors", back_populates="classes")
    class_link = relationship("StudentsClasses", back_populates="link_class")


class Professors(Base):
    __tablename__ = "professors"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    cpf = Column(String, unique=True)
    name = Column(String)
    title = Column(String)

    classes = relationship("Classes", back_populates="professor")


class StudentsClasses(Base):
    __tablename__ = "students_classes"

    student_id = Column(UUID(as_uuid=True), ForeignKey(
        "students.id"), primary_key=True)
    class_id = Column(UUID(as_uuid=True), ForeignKey(
        "classes.id"), primary_key=True)
    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)

    link_student = relationship("Students", back_populates="student_link")
    link_class = relationship("Classes", back_populates="class_link")
